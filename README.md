# Python + Flask + Docker
This is a Python web application using Flask connected to PostgreSQL DB and build in docker
https://www.youtube.com/watch?v=dam0GPOAvVI&ab_channel=TechWithTim
58
- SQL start at 1h 13m
- Notes  start at 1h 59m
- delete notes start at 2h 05m

Jenkins CI/CD
https://www.youtube.com/watch?v=mszE-OCI2V4&ab_channel=JavaTechie

## Install pip
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py
```

- install dependencies
```
pip3 install flask
pip3 install flask_sqlalchemy
pip3 install flask_login
pip3 install pymysql
pip3 install psycopg2
```

- Generate Requirements
```shell
pip3 freeze > requirements.txt
pip3 install -r requirements.txt
```

# Docker
- Build image
```
docker build -t website:1.0 .
```

- Tag build image
```
docker tag website:1.0 efcanchari/website:1.0
```

- Push image
```
docker push efcanchari/website:1.0
```